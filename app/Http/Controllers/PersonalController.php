<?php

namespace App\Http\Controllers;

use App\Models\Personal;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PersonalController extends Controller
{
    public function getPersonal($id_establecimiento)
    {
        $personales = Personal::where('establecimiento_id', $id_establecimiento)->with('user')->get();
        return response()->json($personales);
    }

    public function getPersonalSimple($id_establecimiento)
    {
        $personales = Personal::where('establecimiento_id', $id_establecimiento)->get();
        return response()->json($personales);
    }

    public function store(Request $request, $id_establecimiento)
    {
        $user = User::create([
            'email' => $request['email'],
            'type' => 3,
            'password' => Hash::make($request['password']),
        ]);

        $user->personal()->create([
            'rut' => $request['rut'],
            'nombre' => $request['nombre'],
            'apellido_paterno' => $request['apellido_paterno'],
            'apellido_materno' => $request['apellido_materno'],
            'telefono' => $request['telefono'],
            'especialidad' => $request['especialidad'],
            'establecimiento_id' => $id_establecimiento,
        ]);

        $personal = $user->personal;
        $personal->user;
        return response()->json($personal);
    }

    public function update(Request $request, $id)
    {
        $personal = Personal::find($id);

        $personal->update([
            'rut' => $request['rut'],
            'nombre' => $request['nombre'],
            'apellido_paterno' => $request['apellido_paterno'],
            'apellido_materno' => $request['apellido_materno'],
            'telefono' => $request['telefono'],
            'especialidad' => $request['especialidad'],
        ]);

        $user = $personal->user;

        if (strlen($request['password'] != 0)) {
            $user->update([
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
            ]);
        } else {
            $user->update([
                'email' => $request['email'],
            ]);
        }

        $personal->user;
        return response()->json($personal);
    }

    public function delete($id)
    {
        $personal = Personal::find($id);

        $user = $personal->user;

        $user->personal()->delete();
        $user->delete();

        return response(true);
    }
}
