<?php

namespace App\Http\Controllers;

use App\Models\Propietario;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    //
    public function register(Request $request)
    {

        $user = User::create([
            'email' => $request['email'],
            'type' => $request['type'],
            'password' => Hash::make($request['password']),
        ]);

        if ($request['type'] == 1) {
            $user->propietario()->create([
                'rut' => $request['rut'],
                'nombre' => $request['nombre'],
                'apellido_paterno' => $request['apellido_paterno'],
                'apellido_materno' => $request['apellido_materno'],
                'telefono' => $request['telefono'],
            ]);
        } else if ($request['type'] == 2) {
            $user->cliente()->create([
                'rut' => $request['rut'],
                'nombre' => $request['nombre'],
                'apellido_paterno' => $request['apellido_paterno'],
                'apellido_materno' => $request['apellido_materno'],
                'telefono' => $request['telefono'],
            ]);
        }

        $token = $user->createToken('auth_token')->plainTextToken;
        $this->detail($user);

        return response()->json([
            'token' => $token,
            'token_type' => 'Bearer',
            'user' => $user,
        ]);
    }

    public function login(Request $request)
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return response()->json([
                'message' => 'Invalid login details'
            ], 401);
        }

        $user = User::where('email', $request['email'])->firstOrFail();

        $token = $user->createToken('auth_token')->plainTextToken;

        $this->detail($user);

        return response()->json([
            'token' => $token,
            'token_type' => 'Bearer',
            'user' => $user,
        ]);
    }

    public function me(Request $request)
    {
        $user = $request->user();

        $this->detail($user);

        return $user;
    }

    public function update(Request $request, $id)
    {
        $user =  User::find($id);

        if (strlen($request['password'] != 0)) {
            $user->update([
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
            ]);
        } else {
            $user->update([
                'email' => $request['email'],
            ]);
        }

        $personalData = "";
        switch ($request['type']) {
            case 1:
                $personalData = $user->propietario;
                break;
            case 3:
                $personalData = $user->personal;
                break;
            default:
                $personalData = $user->cliente;
                break;
        }

        $personalData->update([
            'rut' => $request['rut'],
            'nombre' => $request['nombre'],
            'apellido_paterno' => $request['apellido_paterno'],
            'apellido_materno' => $request['apellido_materno'],
            'telefono' => $request['telefono'],
        ]);

        $this->detail($user);
        
        return response()->json($user);
    }


    public function detail($user)
    {
        switch ($user['type']) {
            case 1:
                $user->propietario;
                $establecimiento = $user->propietario->establecimiento;
                $user['establecimiento'] = $establecimiento;
                break;
            case 2:
                $user->cliente;
                break;
            case 3:
                $user->personal;
                $establecimiento = $user->personal->establecimiento;
                $user['establecimiento'] = $establecimiento;
                break;
        }
    }
}
