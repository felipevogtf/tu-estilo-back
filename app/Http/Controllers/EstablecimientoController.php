<?php

namespace App\Http\Controllers;

use App\Models\Direccion;
use App\Models\Establecimiento;
use App\Models\Puntuacion;
use App\Models\Tipo_Establecimiento;
use App\Models\Ventas;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EstablecimientoController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $direccion_data = json_decode($request->direccion);
        $direccion = Direccion::create([
            'nombre' => $direccion_data->nombre,
            'numero' => $direccion_data->numero,
            'ciudad' => $direccion_data->ciudad,
            'region' => $direccion_data->region,
        ]);

        $file_path = $this->uploadImg($request);

        $direccion->establecimiento()->create([
            'nombre' => $request->nombre,
            'telefono' => $request->telefono,
            'descripcion' => $request->descripcion,
            'capacidad' => $request->capacidad,
            'capacidad_diaria' => $request->capacidad_diaria,
            'hora_inicio' => $request->hora_inicio,
            'hora_fin' => $request->hora_fin,
            'tipo_establecimiento_id' => $request->tipo_establecimiento_id,
            'propietario_id' => $request->propietario_id,
            'img' => $file_path,
        ]);

        $establecimiento = $direccion->establecimiento;

        $establecimiento->direccion;
        $establecimiento->tipo_establecimiento;
        $establecimiento->puntuaciones = Puntuacion::where('establecimiento_id', $establecimiento->id)->with('cliente')->get();;
        $establecimiento->promedio = $establecimiento->puntuaciones()->avg('puntuacion');

        return response()->json($establecimiento);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $establecimiento = Establecimiento::find($id);

        $direccion_data = json_decode($request->direccion);

        $establecimiento->direccion()->update([
            'nombre' => $direccion_data->nombre,
            'numero' => $direccion_data->numero,
            'ciudad' => $direccion_data->ciudad,
            'region' => $direccion_data->region,
        ]);

        if ($request->editImg == 'false') {
            $file_path = $this->uploadImg($request);
            $establecimiento->update([
                'img' => $file_path,
            ]);
        }

        $establecimiento->update([
            'nombre' => $request->nombre,
            'telefono' => $request->telefono,
            'descripcion' => $request->descripcion,
            'capacidad' => $request->capacidad,
            'capacidad_diaria' => $request->capacidad_diaria,
            'hora_inicio' => $request->hora_inicio,
            'hora_fin' => $request->hora_fin,
            'tipo_establecimiento_id' => $request->tipo_establecimiento_id,
        ]);

        $establecimiento->direccion;
        $establecimiento->tipo_establecimiento;
        $establecimiento->puntuaciones = Puntuacion::where('establecimiento_id', $id)->with('cliente')->get();;
        $establecimiento->promedio = $establecimiento->puntuaciones()->avg('puntuacion');

        return response()->json($establecimiento);
    }

    public function getEstablecimiento($id)
    {
        $establecimiento = Establecimiento::find($id);
        if ($establecimiento) {
            $establecimiento->direccion;
            $establecimiento->tipo_establecimiento;
            $establecimiento->puntuaciones = Puntuacion::where('establecimiento_id', $id)->with('cliente')->get();;
            $establecimiento->promedio = $establecimiento->puntuaciones()->avg('puntuacion');
        }
        return response($establecimiento);
    }

    public function getEstablecimientos()
    {
        $establecimientos = Establecimiento::with('direccion', 'tipo_establecimiento')->where('estado', 1)->orderBy('estado_premium', 'desc')->get();
        $establecimientos = $this->getAvg($establecimientos);
        return response()->json($establecimientos);
    }

    public function getEstablecimientosVencidos()
    {
        $establecimientos = Establecimiento::where('duracion_plan', '<', Carbon::now())->get();
        $establecimientos = $this->getAvg($establecimientos);
        return response()->json($establecimientos);
    }

    public function getEstablecimientosVencidosPremium()
    {
        $establecimientos = Establecimiento::where('duracion_plan_premium', '<', Carbon::now())->get();
        $establecimientos = $this->getAvg($establecimientos);
        return response()->json($establecimientos);
    }

    public function desactivar($id)
    {
        $establecimiento = Establecimiento::find($id);
        $establecimiento->update([
            'estado' => 0,
            'duracion_plan' => null,
        ]);
        return response()->json($establecimiento);
    }

    public function desactivarPremium($id)
    {
        $establecimiento = Establecimiento::find($id);
        $establecimiento->update([
            'estado_premium' => 0,
            'duracion_plan_premium' => null,
        ]);
        return response()->json($establecimiento);
    }

    public function ventas($id, Request $request)
    {
        $establecimiento = Establecimiento::find($id);
        $total = $this->ventasTotal($establecimiento);
        $mes = $this->ventasMensual($establecimiento);
        $dia = $this->ventasDia($establecimiento);
        $reporte_semanal = $this->ventasSemanales($establecimiento);
        $reporte_mensual = $this->ventasMensuales($establecimiento, $request->mes);
        return response()->json([
            'total' => $total,
            'mes' => $mes,
            'dia' => $dia,
            'reporte_semanal' => $reporte_semanal,
            'reporte_mensual' => $reporte_mensual,
        ]);
    }

    public function ventasTotal($establecimiento)
    {
        $ventas = $establecimiento->ventas->sum('total');
        return $ventas;
    }

    public function ventasMensual($establecimiento)
    {
        $ventas = $establecimiento->ventas->where('fecha_venta', '>=', Carbon::now()->subDays(30))->sum('total');
        return $ventas;
    }

    public function ventasDia($establecimiento)
    {
        $ventas = $establecimiento->ventas->where('fecha_venta', '>=', Carbon::now()->subHours(28))->sum('total');
        return $ventas;
    }

    public function ventasSemanales($establecimiento)
    {
        $ventas = Ventas::select(
            DB::raw('sum(total) as ventas'),
            DB::raw("DATE_FORMAT(fecha_venta,'%W-%d') as dia")
        )
            ->where('establecimiento_id', $establecimiento->id)
            ->where('fecha_venta', '<=', Carbon::now())
            ->where('fecha_venta', '>=', Carbon::now()->subDays(7))
            ->orderBy('fecha_venta', "asc")
            ->groupBy('dia')
            ->get();

        $reporte = [['Dia', 'Ventas']];

        if (count($ventas) == 0) {
            return [];
        }
        
        foreach ($ventas as $key => $venta) {
            $dia = explode("-", $venta->dia);
            $reporte[] = [$this->diasEsp($dia[0]) . " " . $dia[1], intval($venta->ventas)];
        }

        return $reporte;
    }

    public function ventasMensuales($establecimiento, $mes)
    {
        $ventas = Ventas::select(
            DB::raw('sum(total) as ventas'),
            DB::raw("DATE_FORMAT(fecha_venta, '%d') as dia")
        )
            ->where('establecimiento_id', $establecimiento->id)
            ->whereMonth('fecha_venta', $mes)
            ->groupBy('dia')
            ->get();

        $reporte = [['Dia', 'Ventas']];

        if (count($ventas) == 0) {
            return [];
        }

        foreach ($ventas as $key => $venta) {
            $reporte[] = [$venta->dia, intval($venta->ventas)];
        }

        return $reporte;
    }

    public function getAvg($establecimientos)
    {
        foreach ($establecimientos as $establecimiento) {
            $establecimiento->promedio = $establecimiento->puntuaciones()->avg('puntuacion');
        }
        return $establecimientos;
    }

    public function getTiposEstablecimientos()
    {
        $tipos_establecimientos = Tipo_Establecimiento::all();
        return response($tipos_establecimientos);
    }

    public function uploadImg(Request $request)
    {
        $file_path = '';
        if ($request->file()) {
            $file_name = time() . '-' . $request->file('img')->getClientOriginalName();;
            $file_path = $request->file('img')->storeAs('images/establecimientos', $file_name, 'public');
        }

        return $file_path;
    }

    public function diasEsp($dia)
    {
        $dias_semana = array(
            "Monday"    => "Lunes",
            "Tuesday"   => "Martes",
            "Wednesday" => "Miércoles",
            "Thursday"  => "Jueves",
            "Friday"    => "Viernes",
            "Saturday"  => "Sábado",
            "Sunday"    => "Domingo",
        );

        return $dias_semana[$dia];
    }
}
