<?php

namespace App\Http\Controllers;

use App\Mail\NotificarSolicitud;
use App\Models\Solicitud;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SolicitudController extends Controller
{
    public function getSolicitudes()
    {
        $planes = Solicitud::where('estado', 0)->with('establecimiento', 'plan')->get();
        return response()->json($planes);
    }

    public function store(Request $request, $id_establecimiento)
    {
        $solicitud = Solicitud::create([
            'establecimiento_id' => $id_establecimiento,
            'plan_id' => $request['plan_id'],
        ]);
        return response()->json($solicitud);
    }

    public function aprobar($id)
    {
        $solicitud =  Solicitud::find($id);
        $plan = $solicitud->plan;
        $establecimiento = $solicitud->establecimiento;
        $user = $establecimiento->propietario->user;

        if ($solicitud->estado == 0) {

            $datos_actualizar = array();
            if ($establecimiento->duracion_plan == null) {
                $datos_actualizar['estado'] = 1;
                $datos_actualizar['duracion_plan'] = Carbon::now()->addDays($plan->dias);
            } else {
                $datos_actualizar['estado'] = 1;
                $datos_actualizar['duracion_plan'] = Carbon::parse($establecimiento->duracion_plan)->addDays($plan->dias);
            }

            if ($solicitud->plan_id == 4) {
                if ($establecimiento->duracion_plan_premium == null) {
                    $datos_actualizar['estado_premium'] = 1;
                    $datos_actualizar['duracion_plan_premium'] = Carbon::now()->addDays($plan->dias);
                } else {
                    $datos_actualizar['estado_premium'] = 1;
                    $datos_actualizar['duracion_plan_premium'] = Carbon::parse($establecimiento->duracion_plan)->addDays($plan->dias);
                }
            }

            Mail::to($user->email)->send(new NotificarSolicitud($plan, 1));

            $establecimiento->update($datos_actualizar);

            $solicitud->update([
                'estado' => 1,
            ]);
        }
        return response()->json($solicitud);
    }

    public function rechazar($id)
    {
        $solicitud = Solicitud::find($id);
        $plan = $solicitud->plan;
        $establecimiento = $solicitud->establecimiento;
        $user = $establecimiento->propietario->user;

        Mail::to($user->email)->send(new NotificarSolicitud($plan, 0));

        $solicitud->delete();
        return response(true);
    }
}
