<?php

namespace App\Http\Controllers;

use App\Models\Puntuacion;
use Illuminate\Http\Request;

class PuntuacionController extends Controller
{
    public function getPuntuaciones($id_establecimiento)
    {
        $puntuaciones = Puntuacion::where('establecimiento_id', $id_establecimiento)->get();
        return response()->json($puntuaciones);
    }

    public function store(Request $request, $id_establecimiento)
    {
        $puntuacion = Puntuacion::create([
            'comentario' => $request['comentario'],
            'puntuacion' => $request['puntuacion'],
            'cliente_id' => $request['cliente_id'],
            'establecimiento_id' => $id_establecimiento,
        ]);
        $puntuacion->cliente;
        return response()->json($puntuacion);
    }
}
