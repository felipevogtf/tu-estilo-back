<?php

namespace App\Http\Controllers;

use App\Models\Categoria_Material;
use App\Models\Materiales;
use Illuminate\Http\Request;

class MaterialesController extends Controller
{
    public function getMateriales($id_establecimiento)
    {
        $materiales = Materiales::where('establecimiento_id', $id_establecimiento)->with('categoria_material')->get();
        return response()->json($materiales);
    }

    public function getMaterialesCategorias()
    {
        $categorias = Categoria_Material::all();
        return response()->json($categorias);
    }

    public function store(Request $request, $id_establecimiento)
    {
        $material = Materiales::create([
            'nombre' => $request['nombre'],
            'cantidad' => $request['cantidad'],
            'categoria_material_id' => $request['categoria_material_id'],
            'establecimiento_id' => $id_establecimiento,
        ]);
        $material->categoria_material;
        return response()->json($material);
    }

    public function update(Request $request, $id)
    {
        $material = Materiales::find($id);

        $material->update([
            'nombre' => $request['nombre'],
            'cantidad' => $request['cantidad'],
            'categoria_material_id' => $request['categoria_material_id'],
        ]);
        $material->categoria_material;
        return response()->json($material);
    }

    public function delete($id)
    {
        $material = Materiales::find($id);

        $material->delete();
        return response(true);
    }
}
