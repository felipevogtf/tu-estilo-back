<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    use HasFactory;
    protected $table = 'plan';
    protected $primaryKey = 'id';
    protected $fillable = ['nombre', 'precio', 'dias'];
    public $timestamps = true;
    
    public function solicitudes()
    {
        return $this->hasMany(Solicitud::class);
    }
}
