<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    use HasFactory;
    protected $table = 'direccion';
    protected $primaryKey = 'id';
    protected $fillable = ['nombre', 'numero', 'ciudad', 'region'];
    public $timestamps = true;

    public function establecimiento()
    {
        return $this->hasOne(Establecimiento::class);
    }
}
