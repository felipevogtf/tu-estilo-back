<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria_Material extends Model
{
    use HasFactory;
    protected $table = 'categoria_material';
    protected $primaryKey = 'id';
    protected $fillable = ['categoria'];
    public $timestamps = true;

    public function materiales()
    {
        return $this->hasMany(Materiales::class);
    }
}
