<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    use HasFactory;
    protected $table = 'solicitud_plan';
    protected $primaryKey = 'id';
    protected $fillable = ['plan_id', 'establecimiento_id', 'estado'];
    public $timestamps = true;
    
    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }
    public function establecimiento()
    {
        return $this->belongsTo(Establecimiento::class);
    }
}
