<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Propietario extends Model
{
    use HasFactory;
    protected $table = 'propietario';
    protected $primaryKey = 'id';
    protected $fillable = ['rut', 'nombre', 'apellido_paterno', 'apellido_materno', 'telefono', 'user_id'];
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function establecimiento()
    {
        return $this->hasOne(Establecimiento::class);
    }
}
