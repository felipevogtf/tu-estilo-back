<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    use HasFactory;
    protected $table = 'cliente';
    protected $primaryKey = 'id';
    protected $fillable = ['rut', 'nombre', 'apellido_paterno', 'apellido_materno', 'telefono', 'user_id'];
    public $timestamps = true;

    public function puntuaciones()
    {
        return $this->hasOne(Puntuacion::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function horas_reservadas()
    {
        return $this->hasMany(Agendar_Hora::class);
    }
}
