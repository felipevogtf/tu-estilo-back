<?php

namespace App\Mail;

use App\Models\Plan;
use App\Models\Solicitud;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotificarSolicitud extends Mailable
{
    use Queueable, SerializesModels;

    public $plan;
    public $estado;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Plan $plan, $estado)
    {
        $this->plan = $plan;
        $this->estado = $estado;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->estado == 1){
            return $this->subject('Solicitud confirmada')->view('mails.notificar_solicitud')->with([
                'titulo' => 'Solicitud confirmada',
                'estado_plan' => 'Confirmada',
                'nombre' => $this->plan->nombre,
                'duracion' => $this->plan->dias,
            ]);
        }else{
            return $this->subject('Solicitud rechazada')->view('mails.notificar_solicitud')->with([
                'titulo' => 'Solicitud rechazada',
                'estado_plan' => 'Rechazada',
                'nombre' => $this->plan->nombre,
                'duracion' => $this->plan->dias,
            ]);
        }
    }
}
