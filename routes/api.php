<?php

use App\Http\Controllers\AgendaController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\EstablecimientoController;
use App\Http\Controllers\MaterialesController;
use App\Http\Controllers\PersonalController;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\PuntuacionController;
use App\Http\Controllers\ServicioController;
use App\Http\Controllers\SolicitudController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VentasController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/** Establecimiento crud */
Route::post('/establecimiento', [EstablecimientoController::class, 'store']);
Route::post('/establecimiento/{establecimiento}', [EstablecimientoController::class, 'update']);
Route::get('/establecimiento/{establecimiento}', [EstablecimientoController::class, 'getEstablecimiento']);
Route::get('/tipos_establecimientos', [EstablecimientoController::class, 'getTiposEstablecimientos']);
Route::get('/establecimientos', [EstablecimientoController::class, 'getEstablecimientos']);

/** Materiales crud */
Route::get('/establecimiento/{establecimiento}/materiales', [MaterialesController::class, 'getMateriales']);
Route::post('/establecimiento/{establecimiento}/materiales', [MaterialesController::class, 'store']);
Route::put('/materiales/{material}', [MaterialesController::class, 'update']);
Route::delete('/materiales/{material}', [MaterialesController::class, 'delete']);
Route::get('/categorias_materiales', [MaterialesController::class, 'getMaterialesCategorias']);

/** Personal crud */
Route::get('/establecimiento/{establecimiento}/personal', [PersonalController::class, 'getPersonal']);
Route::post('/establecimiento/{establecimiento}/personal', [PersonalController::class, 'store']);
Route::put('/personal/{personal}', [PersonalController::class, 'update']);
Route::delete('/personal/{personal}', [PersonalController::class, 'delete']);

/** Servicios crud */
Route::get('/establecimiento/{establecimiento}/personalSimple', [PersonalController::class, 'getPersonalSimple']);
Route::get('/establecimiento/{establecimiento}/servicios', [ServicioController::class, 'getServicios']);
Route::post('/establecimiento/{establecimiento}/servicios', [ServicioController::class, 'store']);
Route::delete('/servicio/{servicio}', [ServicioController::class, 'delete']);
Route::post('/servicio/{servicio}', [ServicioController::class, 'update']);

/** Agenda*/
Route::post('/agenda/obtenerHorasDisponibles', [AgendaController::class, 'obtenerHorasDisponibles']);
Route::post('/agenda/reservarHora', [AgendaController::class, 'reservarHora']);
Route::get('/establecimiento/{establecimiento}/reservas', [AgendaController::class, 'getReservas']);
Route::get('/establecimiento/{establecimiento}/reservasConfirmadas', [AgendaController::class, 'getReservasConfirmadas']);
Route::post('/reserva/{reserva}/confirmar', [AgendaController::class, 'confirmarReserva']);
Route::get('/cliente/{cliente}/reservas', [AgendaController::class, 'getReservasCliente']);

/** Ventas crud */
Route::get('/establecimiento/{establecimiento}/ventas', [VentasController::class, 'getVentas']);
Route::post('/establecimiento/{establecimiento}/estadisticas', [EstablecimientoController::class, 'ventas']);
Route::post('/establecimiento/{establecimiento}/ventas', [VentasController::class, 'store']);
Route::put('/ventas/{venta}', [VentasController::class, 'update']);
Route::delete('/ventas/{venta}', [VentasController::class, 'delete']);

/** Puntuaciones */
Route::get('/establecimiento/{establecimiento}/puntuacion', [PuntuacionController::class, 'getPuntuaciones']);
Route::post('/establecimiento/{establecimiento}/puntuacion', [PuntuacionController::class, 'store']);

/** Planes */
Route::get('/planes', [PlanController::class, 'getPlanes']);

/** Solicitudes */
Route::get('/solicitudes', [SolicitudController::class, 'getSolicitudes']);
Route::post('/establecimiento/{establecimiento}/solicitud', [SolicitudController::class, 'store']);
Route::post('/solicitud/{solicitud}', [SolicitudController::class, 'aprobar']);
Route::delete('/solicitud/{solicitud}', [SolicitudController::class, 'rechazar']);


/** Admin */
Route::post('/establecimiento/{establecimiento}/desactivar', [EstablecimientoController::class, 'desactivar']);
Route::get('/establecimientos/vencidos', [EstablecimientoController::class, 'getEstablecimientosVencidos']);
Route::post('/establecimiento/{establecimiento}/desactivarPremium', [EstablecimientoController::class, 'desactivarPremium']);
Route::get('/establecimientos/vencidosPremium', [EstablecimientoController::class, 'getEstablecimientosVencidosPremium']);


Route::post('/register', [AuthController::class, 'register']);
Route::put('/user/{user}', [AuthController::class, 'update']);
Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::get('/me', [AuthController::class, 'me'])->middleware('auth:sanctum');
